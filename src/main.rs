
use eframe::{run_native, epi, egui::{self, Button, RichText}, epi::App, epaint::{FontId, Color32}};

struct Cell {
	a1: RichText,
	a2: RichText,
	a3: RichText,

	a1_used: bool,
	a2_used: bool,
	a3_used: bool,

	b1: RichText,
	b2: RichText,
	b3: RichText,

	b1_used: bool,
	b2_used: bool,
	b3_used: bool,

	c1: RichText,
	c2: RichText,
	c3: RichText,

	c1_used: bool,
	c2_used: bool,
	c3_used: bool,
}

struct MyApp<'a> {
	cell: Cell,
	history: Vec<&'a str>,
	player1_turn: bool,
	undo_error_label: RichText
}

fn return_turn(player1_turn: &mut bool) -> RichText {
	if *player1_turn {
		*player1_turn = !(*player1_turn);
		RichText::new("X").font(FontId::proportional(20.0))
	} else {
		*player1_turn = !(*player1_turn);
		RichText::new("O").font(FontId::proportional(20.0))
	}
}

// undo the last action
fn undo(cell: &mut Cell, history: &mut Vec<&str>) -> Result<(), RichText> {
	match history.last() {
		Some(cellstr) => {
			// see which cell should be undo'ed
			match *cellstr {
				"a1" => { cell.a1 = RichText::new("_"); cell.a1_used = false },
				"a2" => { cell.a2 = RichText::new("_"); cell.a2_used = false },
				"a3" => { cell.a3 = RichText::new("_"); cell.a3_used = false },

				"b1" => { cell.b1 = RichText::new("_"); cell.b1_used = false },
				"b2" => { cell.b2 = RichText::new("_"); cell.b2_used = false },
				"b3" => { cell.b3 = RichText::new("_"); cell.b3_used = false },

				"c1" => { cell.c1 = RichText::new("_"); cell.c1_used = false },
				"c2" => { cell.c2 = RichText::new("_"); cell.c2_used = false },
				"c3" => { cell.c3 = RichText::new("_"); cell.c3_used = false },
				_ => return Err(RichText::new("Not in History"))
			}
		},
		None => return Err(RichText::new("ERROR: No action").color(Color32::LIGHT_RED)),
	};

	// delete the undo'ed cell
	history.pop();
	Ok(())
}

fn new_game(cell: &mut Cell) {
	cell.a1 = RichText::new("_");
	cell.a2 = RichText::new("_");
	cell.a3 = RichText::new("_");
	cell.a1_used = false;
	cell.a2_used = false;
	cell.a3_used = false;

	cell.b1 = RichText::new("_");
	cell.b2 = RichText::new("_");
	cell.b3 = RichText::new("_");
	cell.b1_used = false;
	cell.b2_used = false;
	cell.b3_used = false;

	cell.c1 = RichText::new("_");
	cell.c2 = RichText::new("_");
	cell.c3 = RichText::new("_");
	cell.c1_used = false;
	cell.c2_used = false;
	cell.c3_used = false;
}

impl App for MyApp<'static> {
	fn update(&mut self, ctx: &egui::Context, frame: &epi::Frame) {
		egui::CentralPanel::default().show(ctx, |ui| {
			// first row buttons
			ui.horizontal(|ui| {
				ui.add_space(100.0);
				if ui.add_sized([100.0, 100.0], Button::new(self.cell.a1.clone())).clicked() {
					if !self.cell.a1_used {
    					self.cell.a1 = return_turn(&mut self.player1_turn);
						self.cell.a1_used = true;
						self.history.push("a1");
						self.undo_error_label = RichText::new("");
					}
				}

				ui.add_space(25.0);
				if ui.add_sized([100.0, 100.0], Button::new(self.cell.b1.clone())).clicked() {
					if !self.cell.b1_used {
						self.cell.b1 = return_turn(&mut self.player1_turn);
						self.cell.b1_used = true;
						self.history.push("b1");
						self.undo_error_label = RichText::new("");
					}
				}

				ui.add_space(25.0);
				if ui.add_sized([100.0, 100.0], Button::new(self.cell.c1.clone())).clicked() {
					if !self.cell.c1_used {
						self.cell.c1 = return_turn(&mut self.player1_turn);
						self.cell.c1_used = true;
						self.history.push("c1");
						self.undo_error_label = RichText::new("");
					}
				}

			});
			ui.add_space(10.0);

			// second row buttons:
			ui.horizontal(|ui| {
				ui.add_space(100.0);
				if ui.add_sized([100.0, 100.0], Button::new(self.cell.a2.clone())).clicked() {
					if !self.cell.a2_used {
    					self.cell.a2 = return_turn(&mut self.player1_turn);
						self.cell.a2_used = true;
						self.history.push("a2");
						self.undo_error_label = RichText::new("");
					}
				}

				// between row 2 buttons' space
				ui.add_space(25.0);
				if ui.add_sized([100.0, 100.0], Button::new(self.cell.b2.clone())).clicked() {
					if !self.cell.b2_used {
						self.cell.b2 = return_turn(&mut self.player1_turn);
						self.cell.b2_used = true;
						self.history.push("b2");
							self.undo_error_label = RichText::new("");
						}
					}

					ui.add_space(25.0);
					if ui.add_sized([100.0, 100.0], Button::new(self.cell.c2.clone())).clicked() {
						if !self.cell.c2_used {
							self.cell.c2 = return_turn(&mut self.player1_turn);
							self.cell.c2_used = true;
							self.history.push("c2");
							self.undo_error_label = RichText::new("");
						}
					}

				});
				ui.add_space(10.0);

				// third row buttons:
				ui.horizontal(|ui| {
									ui.add_space(100.0);
					if ui.add_sized([100.0, 100.0], Button::new(self.cell.a3.clone())).clicked() {
						if !self.cell.a3_used {
    						self.cell.a3 = return_turn(&mut self.player1_turn);
							self.cell.a3_used = true;
							self.history.push("a3");
							self.undo_error_label = RichText::new("");
						}
					}

					ui.add_space(25.0);
					if ui.add_sized([100.0, 100.0], Button::new(self.cell.b3.clone())).clicked() {
						if !self.cell.b3_used {
							self.cell.b3 = return_turn(&mut self.player1_turn);
							self.cell.b3_used = true;
							self.history.push("b3");
							self.undo_error_label = RichText::new("");
						}
					}

					ui.add_space(25.0);
					if ui.add_sized([100.0, 100.0], Button::new(self.cell.c3.clone())).clicked() {
						if !self.cell.c3_used {
							self.cell.c3 = return_turn(&mut self.player1_turn);
							self.cell.c3_used = true;
							self.history.push("c3");
							self.undo_error_label = RichText::new("");
						}
					}

				});
				ui.add_space(50.0);

				// New game button and Undo button & its label
				ui.horizontal(|ui| {
					ui.add_space(100.0);
					if ui.button(RichText::new("New Game").font(FontId::proportional(16.0))).clicked() {
						new_game(&mut self.cell);
					}
					ui.add_space(10.0);
					if ui.button(RichText::new("Undo").font(FontId::proportional(16.0))).clicked() {
						// undo if pressed
						match undo(&mut self.cell, &mut self.history) {
							Ok(_) => (),
							Err(e) => {
								self.undo_error_label = e;
							}
						}
					}
					ui.label(self.undo_error_label.clone());

				});
			});
		}

		fn name(&self) -> &str {
			"Test"
		}
	}

	impl Default for Cell {
			fn default() -> Self {
				Cell {
			a1: RichText::new("_"),
			a2: RichText::new("_"),
			a3: RichText::new("_"),

			a1_used: false,
			a2_used: false,
			a3_used: false,

			b1: RichText::new("_"),
			b2: RichText::new("_"),
			b3: RichText::new("_"),

			b1_used: false,
			b2_used: false,
			b3_used: false,

			c1: RichText::new("_"),
			c2: RichText::new("_"),
			c3: RichText::new("_"),

			c1_used: false,
			c2_used: false,
			c3_used: false,
		}
	}
}

impl Default for MyApp<'static> {
	fn default() -> Self {
		MyApp {
			cell: Cell::default(),
			history: Vec::new(),
			player1_turn: true,
			undo_error_label: RichText::new(""),
		}
	}
}

fn main() {
	let app = MyApp::default();
	let native_options = eframe::NativeOptions::default();
	run_native(Box::new(app), native_options);
}
